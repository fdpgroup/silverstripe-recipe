# SilverStripe Recipe

```javascript
{
    "name": "fdp/site",
    "description": "Base FDP Site",
    "repositories": [
        {
            "type": "vcs",
            "url": "https://fdpgroup@bitbucket.org/fdpgroup/fdp-silverstripe-common.git"
        },
        {
            "type": "vcs",
            "url": "https://fdpgroup@bitbucket.org/fdpgroup/silverstripe-recipe.git"
        }
    ],
    "authors": [
        {
            "name": "Luke Skelding",
            "email": "luke@kojesolutions.com"
        }
    ],
    "minimum-stability": "dev",
    "require": {
        "fdp/silverstripe-recipe": "dev-master"
    }
}
```
